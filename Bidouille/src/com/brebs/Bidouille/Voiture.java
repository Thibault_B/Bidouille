package com.brebs.Bidouille;

public class Voiture {
	int chassis = 1;
	int roues = 4;
	boolean clim = false;
	public String Type = "Type";

	// Voiture(String Type,int roues,boolean clim,){
	// this.Type= Type;
	// this.roues= roues;
	// this.clim= clim;
	// this.cc=cc;

	// }
	Voiture() {

	}

	public int getRoues() {
		return roues;
	}

	public void setRoues(int roues) {
		this.roues = roues;
	}

	public boolean isClim() {
		return clim;
	}

	public void setClim(boolean clim) {
		this.clim = clim;
	}

	public String getType() {
		return Type;
	}

	public void setType(String type) {
		Type = type;
	}

}
